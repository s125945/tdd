package workshop.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;
public class MatrixUtilityTest {
    @Test
    public void givenInvalidFirstMatrixAndValidSecondMatrix_whenSum_thenExceptionIsThrown() {
        int[][] first = {
                {3, 4, 5},
                {2, 4},
                {3, 5, 6}
        };
        int[][] second = {
                {3, 4, 5},
                {1, 1, 1},
                {3, 4, 5}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sum(first, second));
        Assertions.assertEquals("inconsistent rows in first matrix", exception.getMessage());
    }

    @Test
    public void givenValidFirstMatrixAndInvalidSecondMatrix_whenSum_thenExceptionIsThrown() {
        int[][] first = {
                {3, 4, 5},
                {2, 4, 4},
                {3, 5, 6}
        };
        int[][] second = {
                {3, 4, 5},
                {1, 1, 1},
                {3, 5}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sum(first, second));
        Assertions.assertEquals("inconsistent rows in second matrix", exception.getMessage());
    }

    @Test
    public void givenNullRowInFirstMatrixAndValidSecondMatrix_whenSum_thenExceptionIsThrown() {
        int[][] first = {
                {3, 4, 5},
                null,
                {3, 5, 6}
        };
        int[][] second = {
                {3, 4, 5},
                {1, 1, 1},
                {3, 5, 3}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sum(first, second));
        Assertions.assertEquals("first matrix contains null row(s)", exception.getMessage());
    }

    @Test
    public void givenValidFirstMatrixAndNullRowInSecondMatrix_whenSum_thenExceptionIsThrown() {
        int[][] first = {
                {1, 1, 1},
                {3, 3, 3},
                {4, 5, 6}
        };
        int[][] second = {
                {2, 2, 2},
                {4, 5, 6},
                null
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sum(first, second));
        Assertions.assertEquals("second matrix contains null row(s)", exception.getMessage());

    }

    @Test
    public void givenValidMatricesWithDifferentSizes_whenSum_thenExceptionIsThrown() {
        int[][] first = {
                {3, 4},
                {2, 4},
        };
        int[][] second = {
                {3, 4, 5},
                {1, 1, 1},
                {3, 4, 5}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sum(first, second));
        Assertions.assertEquals("first and second matrices are of different sizes", exception.getMessage());

    }

    @Test
    public void givenTwoValidMatrices_whenSum_thenExpectedResultIsReturned() {
        int[][] left = {
                {1, 1, 1},
                {3, 3, 3},
                {4, 5, 6}
        };
        int[][] right = {
                {2, 2, 2},
                {4, 5, 6},
                {3, 3, 4}
        };

        int[][] result = MatrixUtility.sum(left, right);

        int[][] expectedResult = {
                {3, 3, 3},
                {7, 8, 9},
                {7, 8, 10}
        };
        Assertions.assertArrayEquals(expectedResult, result, "returned result not as expected");
    }

    @Test
    public void givenInvalidMatrix_whenTranspose_thenThrowException() {
        // scenario, still not exists
        int[][] nullRowMatrix = {
                null,
                {3, 4, 5},
                {4, 6, 7}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.transpose(nullRowMatrix));
        Assertions.assertEquals("matrix contains null row(s)", exception.getMessage());

        int[][] inconsistent = {
                {3, 4, 5},
                {4, 3, 5, 5}
        };
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.transpose(inconsistent));
        Assertions.assertEquals("inconsistent rows in matrix", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenTranspose_thenReturnExpectedResult() {
        int[][] matrix = {
                {3, 4, 5},
                {6, 7, 8}
        };

        int[][] transposed = MatrixUtility.transpose(matrix);

        int[][] expected = {
                {3, 6},
                {4, 7},
                {5, 8}
        };
        Assertions.assertArrayEquals(expected, transposed, "unexpected transposed result");
    }

    @Test
    public void givenNullMatrix_whenSubMatrix_thenThrowException() {
        // matrix:int[][], rowToRemove:int, colToRemove:int
        int[][] matrix = null;
        int row = -1;
        int col = -1;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> MatrixUtility.sub(matrix, row, col));
        Assertions.assertEquals("null matrix", exception.getMessage());
    }

    @Test
    public void givenEmptyMatrix_whenSubMatrix_thenThrowException() {
        int[][] matrix = new int[0][];
        int row = -1;
        int col = -1;

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, row, col));
        Assertions.assertEquals("empty matrix", exception.getMessage());
    }

    @Test
    public void givenValidMatrixAndInvalidRow_whenSub_thenThrowException() {
        int[][] matrix = {
                {3, 5, 4},
                {5, 6, 1},
                {9, 6, 2}
        };
        int row = 3;
        int col = 2;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, row, col));
        Assertions.assertEquals("Rows out of bound", exception.getMessage());

        Random random = new Random();
        int rowForSecondTest = (random.nextInt(10) + 1) * -1;
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, rowForSecondTest, col));
        Assertions.assertEquals("Rows out of bound", exception.getMessage());
    }

    @Test
    public void givenValidMatrixAndValidRowAndInvalidCol_whenSub_thenThrowException() {
        int[][] matrix = {
                {3, 5, 4},
                {5, 6, 1},
                {9, 6, 2}
        };
        int row = 2;
        int col = 3;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, row, col));
        Assertions.assertEquals("Column index out of bound", exception.getMessage());

        Random random = new Random();
        int colForSecondTest = (random.nextInt(10) + 1) * -1;
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, row, colForSecondTest));
        Assertions.assertEquals("Column index out of bound", exception.getMessage());
    }

    @Test
    public void givenInvalidMatrix_whenSub_thenThrowException() {
        int[][] matrix = {
                {9, 3, 6},
                null,
                {4, 5, 6}
        };
        int row = 0;
        int col = 0;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(matrix, row, col));
        Assertions.assertEquals("matrix contains null row(s)", exception.getMessage());
        int[][] secondMatrix = {
                {9, 6, 5},
                {4, 5},
                {4, 8, 7}
        };
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.sub(secondMatrix, row, col));
        Assertions.assertEquals("inconsistent rows in matrix", exception.getMessage());
    }

    @Test
    public void givenValidInputs_whenSub_thenReturnValidResult() {
        int[][] matrix = {
                {4, 2, 3, 1},
                {4, 1, 3, 6},
                {1, 5, 6, 8},
                {5, 6, 7, 1}
        };
        int row = 0;
        int col = 0;
        int[][] expectedResult = {
                {1, 3, 6},
                {5, 6, 8},
                {6, 7, 1}
        };
        int[][] actualResult = MatrixUtility.sub(matrix, row, col);
        MatrixUtility.scale(matrix, 10);
        Assertions.assertArrayEquals(expectedResult, actualResult, "Invalid Result");
    }

    @Test
    public void givenMatrix_whenScale_thenReturnScaledMatrix() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9}
        });

        Matrix result = MatrixUtility.scale(matrix, 5);
        Assertions.assertNotNull(result, "result is null");
        Assertions.assertEquals(2, result.getRows(), "invalid rows");
        Assertions.assertEquals(3, result.getCols(), "invalid cols");

        int[][] expected = {
                {15, 25, 30},
                {35, 40, 45}
        };
        for (int row = 0; row < expected.length; row++) {
            for (int col = 0; col < expected[row].length; col++) {
                Assertions.assertEquals(expected[row][col], result.getValue(row, col));
            }
        }
    }

    @Test
    public void givenValidMatrix_whenTranspose_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9}
        });

        Matrix actualResult = MatrixUtility.transpose(matrix);
        Matrix expectedResult = new Matrix(new int[][]{
                {3, 7},
                {5, 8},
                {6, 9}
        });
        Assertions.assertEquals(expectedResult, actualResult, "Result not as expected");

    }

    @Test
    public void givenNotSquareMatrix_whenCopySquareMatrix_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.copySquareMatrix(matrix, CopyType.DIAGONAL));
        Assertions.assertEquals("Error, It's not a square Matrix !!", exception.getMessage());
    }

    @Test
    public void givenValidMatrixWithInvalidCopyType_whenCopySquareMatrix_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9},
                {2, 44, 1}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.copySquareMatrix(matrix, null));
        Assertions.assertEquals("Error, Invalid copy Type!!", exception.getMessage());
    }

    @Test
    public void givenValidMatrixWithDCopyType_whenCopySquareMatrix_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9},
                {2, 44, 1}
        });
        Matrix actualResult = MatrixUtility.copySquareMatrix(matrix, CopyType.DIAGONAL);
        Matrix expectedResult = new Matrix(new int[][]{
                {3, 0, 0},
                {0, 8, 0},
                {0, 0, 1}
        });
        Assertions.assertEquals(expectedResult, actualResult, "Result not as expected");

    }

    @Test
    public void givenValidMatrixWithLCopyType_whenCopySquareMatrix_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9},
                {2, 44, 1}
        });
        Matrix actualResult = MatrixUtility.copySquareMatrix(matrix, CopyType.LOWER);
        Matrix expectedResult = new Matrix(new int[][]{
                {3, 0, 0},
                {7, 8, 0},
                {2, 44, 1}
        });
        Assertions.assertEquals(expectedResult, actualResult, "Result not as expected");

    }

    @Test
    public void givenValidMatrixWithUCopyType_whenCopySquareMatrix_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9},
                {2, 44, 1}
        });

        Matrix actualResult = MatrixUtility.copySquareMatrix(matrix, CopyType.UPPER);
        Matrix expectedResult = new Matrix(new int[][]{
                {3, 5, 6},
                {0, 8, 9},
                {0, 0, 1}
        });
        Assertions.assertEquals(expectedResult, actualResult, "Result not as expected");

    }

    @Test
    public void givenNotSquareMatrix_whenGetDeterminant_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {3, 5, 6},
                {7, 8, 9}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> MatrixUtility.determinant(matrix));
        Assertions.assertEquals("Error, It's not a square Matrix !!", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenGetDeterminant_thenValidResult() {
        Matrix matrix = new Matrix(new int[][]{
                {2, -3, 1},
                {2, 0, -1},
                {1, 4, 5}
        });
        int actualResult = MatrixUtility.determinant(matrix);
        int expectedResult = 49;
        Assertions.assertEquals(expectedResult, actualResult, "Result not as expected");
    }

}
